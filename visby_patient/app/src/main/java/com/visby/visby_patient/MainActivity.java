package com.visby.visby_patient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.islamkhsh.CardSliderViewPager;
import com.google.android.material.snackbar.Snackbar;
import com.visby.visby_patient.adapter.AppointmentAdapter;
import com.visby.visby_patient.adapter.PrescriptionAdapter;
import com.visby.visby_patient.adapter.ResultsAdapter;
import com.visby.visby_patient.interfaceListener.Appointment_Interface;
import com.visby.visby_patient.model.Appointment;
import com.visby.visby_patient.model.GetConsumerVitalsResponse;
import com.visby.visby_patient.model.GetPatientListResponse;
import com.visby.visby_patient.model.InitateCallDTO;
import com.visby.visby_patient.model.Prescription;
import com.visby.visby_patient.model.Result;
import com.visby.visby_patient.network.NetworkConstants;
import com.visby.visby_patient.network.UserNetworkService;
import com.visby.visby_patient.network.ValidateUserUtils;

import org.jitsi.meet.sdk.JitsiMeet;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.jitsi.meet.sdk.JitsiMeetUserInfo;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  implements Appointment_Interface {

    private CardSliderViewPager resultCardSliderViewPager;
    private CardSliderViewPager appointmentCardSliderViewPager;
    private CardSliderViewPager prescriptionCardSliderViewPages;
    private ConstraintLayout parentView;
    ArrayList<Result> mResults;
//    Long episodeId = 2420L;
    private ResultsAdapter mResultsAdapter;
//    private int cardcount = 0;
//    private Long userID = 119033L;
    private TextView name;
    private View view;
    String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

        mResults = new ArrayList<Result>();
        Result result = new Result();
        result.setTestResult("Positive");
        result.setDoctorName("Dr. Shakita");
        result.setAddress("Kaiser Permanente, Wawona, CA");
        mResults.add(result);

        Result result1 = new Result();
        result1.setTestResult("Negative");
        result1.setDoctorName("Dr. Shakita");
        result1.setAddress("1968 Biddie Lane ,Wawona, CA");
        mResults.add(result1);
//
//        Result result2 = new Result();
//        result2.setTestResult("Positive");
//        result2.setDoctorName("Dr. Shakita");
//        result2.setAddress("Kaiser Permanente, Wawona, CA");
//        mResults.add(result2);

        ArrayList<Appointment> mAppointment = new ArrayList<Appointment>();
        Appointment appointment = new Appointment();
        appointment.setName("Dr. Anna, Physician");
        appointment.setDateAndTime("2 pm wednesday, Feb 25");
        appointment.setAddress("Kaiser Permanente ,Wawona, CA");
        mAppointment.add(appointment);

        Appointment appointment2 = new Appointment();
        appointment2.setName("Dr. Kelly Preston, Dermatologists");
        appointment2.setDateAndTime("4 pm wednesday, Feb 28");
        appointment2.setAddress("1968 Biddie Lane ,Wawona, CA");
        mAppointment.add(appointment2);

        Appointment appointment3 = new Appointment();
        appointment3.setName("Dr. John travolta, Neurologists");
        appointment3.setDateAndTime("2 pm wednesday, Mar 10");
        appointment3.setAddress("1455 Kelly Drive ,Wawona, CA");
        mAppointment.add(appointment3);

        ArrayList<Prescription> mPrescription = new ArrayList<Prescription>();
        Prescription prescription = new Prescription();
        prescription.setMedicen("Hydroxychlor 20mg");
        prescription.setConsumption("1 tab 2 times per day");
        prescription.setTest("Covid 19 Test");
        prescription.setPrescribedDoctor("Prescribed by Dr. Shakita");
        mPrescription.add(prescription);

        Prescription prescription2 = new Prescription();
        prescription2.setMedicen("Remdesivir");
        prescription2.setConsumption("1 tab 2 times per day");
        prescription2.setTest("Covid 19 Test");
        prescription2.setPrescribedDoctor("Prescribed by Dr. Kelly Preston");
        mPrescription.add(prescription2);

        Prescription prescription3 = new Prescription();
        prescription3.setMedicen("Hipen Clav 625mg");
        prescription3.setConsumption("1 tab 2 times per day");
        prescription3.setTest("Covid 19 Test");
        prescription3.setPrescribedDoctor("Prescribed by Dr. John travolta");
        mPrescription.add(prescription3);

        // Initialize default options for Jitsi Meet conferences.
        URL serverURL;
        try {
            serverURL = new URL(   "https://wecon.wenzins.live"); //"https://meet.jit.si");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Invalid server URL!");
        }
        JitsiMeetConferenceOptions defaultOptions
                = new JitsiMeetConferenceOptions.Builder()
                .setServerURL(serverURL)
                .setWelcomePageEnabled(false)
                .build();
        JitsiMeet.setDefaultConferenceOptions(defaultOptions);

        // add items to arraylist

        view = findViewById(R.id.custome_action_bar);
        parentView = findViewById(R.id.parent_layout);
        resultCardSliderViewPager = findViewById(R.id.results_viewPager);
        mResultsAdapter = new ResultsAdapter(mResults);
        resultCardSliderViewPager.setAdapter(mResultsAdapter);

        appointmentCardSliderViewPager = findViewById(R.id.appointments_viewPager);
        appointmentCardSliderViewPager.setAdapter(new AppointmentAdapter(mAppointment,MainActivity.this));

        prescriptionCardSliderViewPages = (CardSliderViewPager) findViewById(R.id.prescription_viewPager);
        prescriptionCardSliderViewPages.setAdapter(new PrescriptionAdapter(mPrescription));

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(getIntent().hasExtra("USER_NAME")) {
            name = view.findViewById(R.id.name);
            userName = getIntent().getExtras().getString("USER_NAME");
            if (!userName.isEmpty()) {
                name.setText("HELLO " + userName + "!");
                if(checkConnection(this)){
                    patientList(userName);
                }

            }
        }

//        if(checkConnection(this)){
////            getConsumerVitals();
//        }
    }

    @Override
    public void onclickApointment() {

        Log.e("MainActivity","Onclick of appointment");
        if(checkConnection(this)){
        initateCall(1123L);
        }else{
            Snackbar.make(parentView,"Please connect to internet",Snackbar.LENGTH_LONG).show();
        }

//        Intent mIntent = new Intent("org.appspot.apprtc.Connect_Activity");
//                    mIntent.putExtra("Name",155889979);
//                    mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//
//                    try {
//                        startActivity(mIntent);
//
//                    } catch (ActivityNotFoundException activityNotFound) {
//
//                    }
    }
    public void getConsumerVitals(long userID, long episodeId){
        String url = NetworkConstants.REST_BASE_URL;
        UserNetworkService dashboardService = ValidateUserUtils.getValidateUserSertvice(url);
        dashboardService.getConsumerVitals(userID,episodeId).enqueue(new Callback<GetConsumerVitalsResponse>() {
            @Override
            public void onResponse(Call<GetConsumerVitalsResponse> call, Response<GetConsumerVitalsResponse> response) {
                if(response.body() != null) {
                    if (response.body().getCode() != null && response.body().getCode().equalsIgnoreCase("200")) {
                        if (response.body().getContent() != null) {
                            if (response.body().getContent().size() > 0) {
//                                cardcount = 0;
                                mResults.clear();
//                                episodeId = episodeId + 1;
//                                userID = userID + 1;
                                for (int index = 0; index < response.body().getContent().size(); index++) {
                                    if (response.body().getContent().get(index).getVitalsName().equalsIgnoreCase("CRP")) {
//                                        if (cardcount == 4) {
//                                            break;
//                                        } else {
                                            Result result = new Result();
                                            result.setTestResult(response.body().getContent().get(index).getVitalsValue());
//                                    result.setTestResult("Positive");
                                            result.setDoctorName("Dr. Shakita");
//                                            result.setDoctorName("Dr. "+userName);
                                            if (index == 0) {
                                                result.setAddress("Kaiser Permanente, Wawona, CA");
                                            } else if (index == 1) {
                                                result.setAddress("1968 Biddie Lane ,Wawona, CA");
                                            } else if (index == 2) {
                                                result.setAddress("1455 Kelly Drive ,Wawona, CA");
                                            } else {
                                                result.setAddress("1968 Biddie Lane ,Wawona, CA");
                                            }
                                            mResults.add(result);
//                                            cardcount = cardcount + 1;
//                                        }
//                                    resultCardSliderViewPager.setAdapter(new ResultsAdapter(mResults));
                                    }
                                }
                                mResultsAdapter.notifyDataSetChanged();

                            }
//                            else {
//                                userID = userID - 1;
//                                episodeId = episodeId - 1;
//                                getConsumerVitals();
//
//
//                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetConsumerVitalsResponse> call, Throwable t) {
                Snackbar.make(parentView,"Server error",Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public void initateCall(Long callProfileId){

//        final int serverKeyId = loginDataBaseHandler.getserverkeyID();
//        int profileId = (int) mSharedPreference.getLong(Constants.PROFILEID,0);
        String url = NetworkConstants.REST_BASE_URL;
        UserNetworkService dashboardService = ValidateUserUtils.getValidateUserSertvice(url);
//        dashboardService.intiateCall(25,true,340,341).enqueue(new Callback<InitateCallDTO>() {
        dashboardService.intiateCall(185,true,660,661).enqueue(new Callback<InitateCallDTO>() {

            @Override
            public void onResponse(Call<InitateCallDTO> call, Response<InitateCallDTO> response) {

                if(response.body() != null) {
                    if (response.body().getCode() != null && response.body().getCode().equalsIgnoreCase("ok")) {

//                        Log.v(TAG, "Video call Start date and time: " + UiUtils.presentDateAndTime());

                        if (response.body().getRoomid() != null && response.body().getContent() != null && response.body().getContent().getWeconurl() != null) {

                            if (response.body().getRoomid().trim().length() > 0) {
                                // Build options object for joining the conference. The SDK will merge the default
                                // one we set earlier and this one when joining.

                                URL serverURL;
                                String room = response.body().getRoomid().trim();  //"https://chat.rachana.com/Mahindrakar" ;
//                        String room = "call_345AKU";
                                String Url = response.body().getContent().getWeconurl().trim();
//                                String name = mUserSharedPreferences.getString(Constants.FIRSTNAME, "");
                                try {
                                    serverURL = new URL(Url); //"https://meet.jit.si");
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();

                                    throw new RuntimeException("Invalid server URL!");
                                }

                                JitsiMeetUserInfo userInfo = new JitsiMeetUserInfo();
//                        userInfo.setEmail("wenzins@gmail.com");
                                userInfo.setDisplayName(userName);

                                JitsiMeetConferenceOptions options
                                        = new JitsiMeetConferenceOptions.Builder()
                                        .setRoom(room)
                                        .setWelcomePageEnabled(false)
                                        .setServerURL(serverURL)
                                        .setUserInfo(userInfo)
//                                .setAudioOnly(audioCallOnly)
                                        .build();

                                // Launch the new activity with the given options. The launch() method takes care
                                // of creating the required Intent and passing the options.
                                try {
                                    JitsiMeetActivity.launch(MainActivity.this, options);
                                } catch (Exception e) {
//                                System.out.println("Exception .....");
                                }
                            }
                        }
                    } else if (response.body().getErrorCode() != null && response.body().getErrorCode().equalsIgnoreCase("400")) {
                        Snackbar.make(parentView, "Something went wrong please try again", Snackbar.LENGTH_LONG).show();
                    } else if (response.body().getErrorCode() != null && response.body().getErrorCode().equalsIgnoreCase("500")) {
                        Snackbar.make(parentView, "Something went wrong please try after some time", Snackbar.LENGTH_LONG).show();
                    }
                }else {
                    Snackbar.make(parentView,"Server error",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<InitateCallDTO> call, Throwable t) {
                Snackbar.make(parentView,"Server error",Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //getting present details.
//        Calendar calendar;
//        SimpleDateFormat dateFormat;
//        String date;
//        calendar = Calendar.getInstance();
//        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        date = dateFormat.format(calendar.getTime());
//        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SERVER_DETAILS, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
//                    editor.putString(Constants.NETWORK_CONNECTED_DATE, date);
//                    editor.putInt(Constants.COUNT,0);
//                    editor.apply();
                    // connected to wifi
                    return true;
                } else {
//                    editor.putString(Constants.NETWORK_CONNECTED_DATE, date);
//                    editor.putInt(Constants.COUNT,0);
//                    editor.apply();
                    return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
                }
            }
        }
        return false;
    }

    private void patientList(String searchParameter){

        String url = NetworkConstants.REST_BASE_URL;
        try {
        UserNetworkService dashboardService = ValidateUserUtils.getValidateUserSertvice(url);
//        dashboardService.intiateCall(25,true,340,341).enqueue(new Callback<InitateCallDTO>() {

            dashboardService.getPatientList(374, searchParameter).enqueue(new Callback<GetPatientListResponse>() {
                @Override
                public void onResponse(Call<GetPatientListResponse> call, Response<GetPatientListResponse> response) {

                    Log.e("Response code", "" + response.body().getCode()+" id "+response.body().getContent().get(0).getPatientId()+" Episode iD : "+ response.body().getContent().get(0).getPatientId());

                if(response.body() != null && response.body().getCode() != null){
                    if(response.body().getCode().equalsIgnoreCase("200")){
                        if(response.body().getContent() != null){
                            for(int index = 0; index <response.body().getContent().size(); index++) {
                                if (response.body().getContent().get(index).getUserId() != 0 &&
                                response.body().getContent().get(index).getEpisodeId() != 0){
                                    getConsumerVitals(response.body().getContent().get(index).getUserId(),
                                            response.body().getContent().get(index).getEpisodeId() );
                                }
                                break;
                            }

                        }
                    }
                }
                }

                @Override
                public void onFailure(Call<GetPatientListResponse> call, Throwable t) {
                    Log.e("Failed", t.getMessage());
                    Snackbar.make(parentView, "Server error", Snackbar.LENGTH_LONG).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Log.e("errorttest",""+e.getMessage());
        }

    }
}