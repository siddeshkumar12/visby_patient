package com.visby.visby_patient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPatientListResponseContent {


    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @SerializedName("patientId")
    @Expose
    private int patientId;

    public int getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }

    @SerializedName("episodeId")
    @Expose
    private int episodeId;

    @SerializedName("userId")
    @Expose
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    //    @SerializedName("patientId")
//    @Expose
//    private Integer patientId;
//    @SerializedName("ccpId")
//    @Expose
//    private Integer ccpId;
//    @SerializedName("userId")
//    @Expose
//    private Integer userId;
//    @SerializedName("episodeId")
//    @Expose
//    private Integer episodeId;
//    @SerializedName("appointmentId")
//    @Expose
//    private Object appointmentId;
//    @SerializedName("ccpEpisodeId")
//    @Expose
//    private Integer ccpEpisodeId;
//    @SerializedName("appointmentStatusId")
//    @Expose
//    private Object appointmentStatusId;
//    @SerializedName("appointmentStatusCode")
//    @Expose
//    private Object appointmentStatusCode;
//    @SerializedName("appointmentStatusName")
//    @Expose
//    private Object appointmentStatusName;
//    @SerializedName("firstName")
//    @Expose
//    private String firstName;
//    @SerializedName("lastName")
//    @Expose
//    private Object lastName;
//    @SerializedName("phone")
//    @Expose
//    private String phone;
//    @SerializedName("dateOfBirth")
//    @Expose
//    private String dateOfBirth;
//    @SerializedName("strDateOfBirth")
//    @Expose
//    private String strDateOfBirth;
//    @SerializedName("gender")
//    @Expose
//    private String gender;
//    @SerializedName("registrationDate")
//    @Expose
//    private Long registrationDate;
//    @SerializedName("strRegistrationDate")
//    @Expose
//    private String strRegistrationDate;
//    @SerializedName("uhid")
//    @Expose
//    private String uhid;
//    @SerializedName("genderId")
//    @Expose
//    private Integer genderId;
//    @SerializedName("age")
//    @Expose
//    private Integer age;
//    @SerializedName("todayDate")
//    @Expose
//    private Object todayDate;
//    @SerializedName("strTodayDate")
//    @Expose
//    private String strTodayDate;
//    @SerializedName("address")
//    @Expose
//    private Object address;
//    @SerializedName("city")
//    @Expose
//    private String city;
//    @SerializedName("waitingTime")
//    @Expose
//    private String waitingTime;
//    @SerializedName("completionDate")
//    @Expose
//    private Object completionDate;
//
//    public Object getId() {
//        return id;
//    }
//
//    public void setId(Object id) {
//        this.id = id;
//    }
//
//    public Integer getPatientId() {
//        return patientId;
//    }
//
//    public void setPatientId(Integer patientId) {
//        this.patientId = patientId;
//    }
//
//    public Integer getCcpId() {
//        return ccpId;
//    }
//
//    public void setCcpId(Integer ccpId) {
//        this.ccpId = ccpId;
//    }
//
//    public Integer getUserId() {
//        return userId;
//    }
//
//    public void setUserId(Integer userId) {
//        this.userId = userId;
//    }
//
//    public Integer getEpisodeId() {
//        return episodeId;
//    }
//
//    public void setEpisodeId(Integer episodeId) {
//        this.episodeId = episodeId;
//    }
//
//    public Object getAppointmentId() {
//        return appointmentId;
//    }
//
//    public void setAppointmentId(Object appointmentId) {
//        this.appointmentId = appointmentId;
//    }
//
//    public Integer getCcpEpisodeId() {
//        return ccpEpisodeId;
//    }
//
//    public void setCcpEpisodeId(Integer ccpEpisodeId) {
//        this.ccpEpisodeId = ccpEpisodeId;
//    }
//
//    public Object getAppointmentStatusId() {
//        return appointmentStatusId;
//    }
//
//    public void setAppointmentStatusId(Object appointmentStatusId) {
//        this.appointmentStatusId = appointmentStatusId;
//    }
//
//    public Object getAppointmentStatusCode() {
//        return appointmentStatusCode;
//    }
//
//    public void setAppointmentStatusCode(Object appointmentStatusCode) {
//        this.appointmentStatusCode = appointmentStatusCode;
//    }
//
//    public Object getAppointmentStatusName() {
//        return appointmentStatusName;
//    }
//
//    public void setAppointmentStatusName(Object appointmentStatusName) {
//        this.appointmentStatusName = appointmentStatusName;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public Object getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(Object lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public String getDateOfBirth() {
//        return dateOfBirth;
//    }
//
//    public void setDateOfBirth(String dateOfBirth) {
//        this.dateOfBirth = dateOfBirth;
//    }
//
//    public String getStrDateOfBirth() {
//        return strDateOfBirth;
//    }
//
//    public void setStrDateOfBirth(String strDateOfBirth) {
//        this.strDateOfBirth = strDateOfBirth;
//    }
//
//    public String getGender() {
//        return gender;
//    }
//
//    public void setGender(String gender) {
//        this.gender = gender;
//    }
//
//    public Long getRegistrationDate() {
//        return registrationDate;
//    }
//
//    public void setRegistrationDate(Long registrationDate) {
//        this.registrationDate = registrationDate;
//    }
//
//    public String getStrRegistrationDate() {
//        return strRegistrationDate;
//    }
//
//    public void setStrRegistrationDate(String strRegistrationDate) {
//        this.strRegistrationDate = strRegistrationDate;
//    }
//
//    public String getUhid() {
//        return uhid;
//    }
//
//    public void setUhid(String uhid) {
//        this.uhid = uhid;
//    }
//
//    public Integer getGenderId() {
//        return genderId;
//    }
//
//    public void setGenderId(Integer genderId) {
//        this.genderId = genderId;
//    }
//
//    public Integer getAge() {
//        return age;
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }
//
//    public Object getTodayDate() {
//        return todayDate;
//    }
//
//    public void setTodayDate(Object todayDate) {
//        this.todayDate = todayDate;
//    }
//
//    public String getStrTodayDate() {
//        return strTodayDate;
//    }
//
//    public void setStrTodayDate(String strTodayDate) {
//        this.strTodayDate = strTodayDate;
//    }
//
//    public Object getAddress() {
//        return address;
//    }
//
//    public void setAddress(Object address) {
//        this.address = address;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public String getWaitingTime() {
//        return waitingTime;
//    }
//
//    public void setWaitingTime(String waitingTime) {
//        this.waitingTime = waitingTime;
//    }
//
//    public Object getCompletionDate() {
//        return completionDate;
//    }
//
//    public void setCompletionDate(Object completionDate) {
//        this.completionDate = completionDate;
//    }
}
