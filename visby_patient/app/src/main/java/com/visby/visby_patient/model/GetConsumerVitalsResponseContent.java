package com.visby.visby_patient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetConsumerVitalsResponseContent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vitalSourceId")
    @Expose
    private String vitalSourceId;
    @SerializedName("vitalSource")
    @Expose
    private String vitalSource;
    @SerializedName("strMonitoredDate")
    @Expose
    private String strMonitoredDate;
    @SerializedName("epsiodeId")
    @Expose
    private Integer epsiodeId;
    @SerializedName("dbmonitoredDate")
    @Expose
    private Long dbmonitoredDate;
    @SerializedName("vitalsName")
    @Expose
    private String vitalsName;
    @SerializedName("vitalsValue")
    @Expose
    private String vitalsValue;
    @SerializedName("vitalsArry")
    @Expose
    private Object vitalsArry;
    @SerializedName("ninamDataArray")
    @Expose
    private Object ninamDataArray;
    @SerializedName("monitoredDate")
    @Expose
    private Long monitoredDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVitalSourceId() {
        return vitalSourceId;
    }

    public void setVitalSourceId(String vitalSourceId) {
        this.vitalSourceId = vitalSourceId;
    }

    public String getVitalSource() {
        return vitalSource;
    }

    public void setVitalSource(String vitalSource) {
        this.vitalSource = vitalSource;
    }

    public String getStrMonitoredDate() {
        return strMonitoredDate;
    }

    public void setStrMonitoredDate(String strMonitoredDate) {
        this.strMonitoredDate = strMonitoredDate;
    }

    public Integer getEpsiodeId() {
        return epsiodeId;
    }

    public void setEpsiodeId(Integer epsiodeId) {
        this.epsiodeId = epsiodeId;
    }

    public Long getDbmonitoredDate() {
        return dbmonitoredDate;
    }

    public void setDbmonitoredDate(Long dbmonitoredDate) {
        this.dbmonitoredDate = dbmonitoredDate;
    }

    public String getVitalsName() {
        return vitalsName;
    }

    public void setVitalsName(String vitalsName) {
        this.vitalsName = vitalsName;
    }

    public String getVitalsValue() {
        return vitalsValue;
    }

    public void setVitalsValue(String vitalsValue) {
        this.vitalsValue = vitalsValue;
    }

    public Object getVitalsArry() {
        return vitalsArry;
    }

    public void setVitalsArry(Object vitalsArry) {
        this.vitalsArry = vitalsArry;
    }

    public Object getNinamDataArray() {
        return ninamDataArray;
    }

    public void setNinamDataArray(Object ninamDataArray) {
        this.ninamDataArray = ninamDataArray;
    }

    public Long getMonitoredDate() {
        return monitoredDate;
    }

    public void setMonitoredDate(Long monitoredDate) {
        this.monitoredDate = monitoredDate;
    }
}
