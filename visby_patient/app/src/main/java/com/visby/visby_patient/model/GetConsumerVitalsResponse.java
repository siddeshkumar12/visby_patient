package com.visby.visby_patient.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetConsumerVitalsResponse {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("content")
    @Expose
    private List<GetConsumerVitalsResponseContent> content = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<GetConsumerVitalsResponseContent> getContent() {
        return content;
    }

    public void setContent(List<GetConsumerVitalsResponseContent> content) {
        this.content = content;
    }
}
