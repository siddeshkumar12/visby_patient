package com.visby.visby_patient.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.islamkhsh.CardSliderAdapter;
import com.visby.visby_patient.R;
import com.visby.visby_patient.model.Prescription;

import java.util.ArrayList;

public class PrescriptionAdapter extends CardSliderAdapter<PrescriptionAdapter.PrescripionViewHolder> {

    private ArrayList<Prescription> mPrescriptions;

    public PrescriptionAdapter(ArrayList<Prescription> prescriptions) {
        mPrescriptions = prescriptions;
    }

    @Override
    public void bindVH(PrescripionViewHolder prescripionViewHolder, int i) {

        prescripionViewHolder.mMedicationName.setText(mPrescriptions.get(i).getMedicen());
        prescripionViewHolder.mMedicationConsumption.setText(mPrescriptions.get(i).getConsumption());
        prescripionViewHolder.mTestResult.setText(mPrescriptions.get(i).getTest());
        prescripionViewHolder.mPrescribedDoctor.setText(mPrescriptions.get(i).getPrescribedDoctor());

    }

    @NonNull
    @Override
    public PrescripionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.prescription_item_view, parent, false);
        return new PrescriptionAdapter.PrescripionViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mPrescriptions.size();
    }

    class PrescripionViewHolder extends RecyclerView.ViewHolder {
        TextView mMedicationName, mMedicationConsumption, mTestResult, mPrescribedDoctor;

        public PrescripionViewHolder(View view) {
            super(view);
            mMedicationName = view.findViewById(R.id.medication_txt);
            mMedicationConsumption = view.findViewById(R.id.medication_consumption);
            mTestResult = view.findViewById(R.id.test_result);
            mPrescribedDoctor = view.findViewById(R.id.prescribed_doctor);
        }
    }
}
