package com.visby.visby_patient.network;

import com.visby.visby_patient.model.GetConsumerVitalsResponse;
import com.visby.visby_patient.model.GetPatientListResponse;
import com.visby.visby_patient.model.InitateCallDTO;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserNetworkService {

    @FormUrlEncoded
    @Headers("deviceId: ADMIN")
    @POST(NetworkConstants.INTIATEANDCLOSECALLV4)
    Call<InitateCallDTO> intiateCall(@Field("serverKeyId") long serverKetId, @Field("initiate") boolean initate,
                                     @Field("profileId") long profileId, @Field("calleProfileId") long calleProfileId);
    @Headers("deviceId: ADMIN")
    @GET(NetworkConstants.GET_CONSUMER_VITALS)
    Call<GetConsumerVitalsResponse>getConsumerVitals(@Query("userId") long userId, @Query("episodeId") long episodeId);

    @GET(NetworkConstants.GET_PATIENT_LIST)
    Call<GetPatientListResponse>getPatientList(@Query("productId") long productId,@Query("searchParameter") String searchParameter);
}
