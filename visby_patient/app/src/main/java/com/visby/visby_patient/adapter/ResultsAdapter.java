package com.visby.visby_patient.adapter;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.islamkhsh.CardSliderAdapter;
import com.visby.visby_patient.R;
import com.visby.visby_patient.model.Result;

import java.util.ArrayList;

public class ResultsAdapter extends CardSliderAdapter<ResultsAdapter.ResultViewHolder> {

    private ArrayList<Result> mResults;

    public ResultsAdapter(ArrayList<Result> movies) {
        this.mResults = movies;
    }

    @Override
    public void bindVH(ResultViewHolder resultViewHolder, int i) {
        String text2 = "Covid Test " +mResults.get(i).getTestResult();

        Spannable spannable = new SpannableString(text2);

        //If a string has 19 characters, the specified color if applicable after 11 characters.

        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#FF3B30")), 11, 19, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        resultViewHolder.mTestResult.setText(spannable, TextView.BufferType.SPANNABLE);

        resultViewHolder.mDoctorName.setText(mResults.get(i).getDoctorName());
        resultViewHolder.mAddress.setText(mResults.get(i).getAddress());

    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        return new ResultViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    class ResultViewHolder extends RecyclerView.ViewHolder {
        private TextView mTestResult, mDoctorName, mAddress;

        public ResultViewHolder(View view) {
            super(view);
            mTestResult = view.findViewById(R.id.test_result);
            mDoctorName = view.findViewById(R.id.doctor_name);
            mAddress = view.findViewById(R.id.address);
        }

    }
}
